﻿namespace oef_23._1.test
{
    internal class DobbelsteenTest
    {
        [Test]
        public void InterneTest()
        {
            //Arrange(dit zijn u variabelen)
            Random random = new Random();
            int rol1;
            int rol2;

            //Act(je voert iets uit)

            rol1 = random.Next();
            rol2 = random.Next();

            //Assert(het verwachte resultaat)
            Assert.IsTrue(rol1 >= 0 && rol2 >= 0);
        }
    }
}
