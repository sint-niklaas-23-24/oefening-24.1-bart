﻿using System;
using System.Windows;

namespace Oef_23._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random mijnRandom = new Random();
        Dobbelsteen mijnDobbelsteen;

        public MainWindow()
        {
            InitializeComponent();

        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mijnDobbelsteen = new Dobbelsteen(4, mijnRandom);
                mijnDobbelsteen.Roll();
                lblBlauw.Content = mijnDobbelsteen.Waarde.ToString();

                mijnDobbelsteen = new Dobbelsteen(mijnRandom);
                mijnDobbelsteen.Roll();
                lblRood.Content = mijnDobbelsteen.Waarde.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Er is een fout opgetreden bij het gooien." , "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            
            //lblBlauw.Content += Convert.ToString(random.Next(1, 9));
            //lblRood.Content += Convert.ToString(random.Next(1, 9));
        }


    }
}
