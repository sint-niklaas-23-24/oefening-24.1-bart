﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef_23._1
{
    internal class Dobbelsteen
    {
        private int _aantalZijden;
        private int _waarde;
        private Random _willGetal;

        public Dobbelsteen() 
        { 
            AantalZijden = 6;
        }
        public Dobbelsteen (int aantalZijden, Random r) 
        {
            AantalZijden = aantalZijden;
            WillGetal = r;
        }
        public Dobbelsteen (Random r) 
        { 
            WillGetal = r;
            AantalZijden = 6;
        }

        public int AantalZijden { get { return _aantalZijden; } set { _aantalZijden = value; } }
        public int Waarde { get { return _waarde; } set { _waarde = value; } }
        public Random WillGetal { get { return _willGetal; } set { _willGetal = value; } }

        public void Roll()
        {
            WillGetal = new Random();
            Waarde = WillGetal.Next(1, AantalZijden + 1);
        }

        
    }
}
